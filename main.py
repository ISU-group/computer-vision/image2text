import cv2
import matplotlib.pyplot as plt
from pathlib import Path
from collections import defaultdict
from skimage.measure import label, regionprops
import numpy as np


def extract_features(image):
    features = []

    labeled = label(image)
    region = regionprops(labeled)[0]

    centroid_normalized = np.array(region.local_centroid) / np.array(region.image.shape)
    features.extend(centroid_normalized)

    features.append(region.eccentricity)
    features.append(region.moments_hu[0])
    features.append(region.extent)

    return features


def mean_letter_distance(regions, epsilon=0.25) -> float:
    dists = np.zeros(len(regions) - 1)
    for i in range(0, len(regions) - 1):
        # distance from X axis
        dists[i] = abs(regions[i].bbox[-1] - regions[i + 1].bbox[1])

    return dists.mean() + epsilon * dists.std()


def get_letter_boundaries(regions, letter_idx):
    if not letter_idx:
        return None

    min_row, min_col, max_row, max_col = regions[letter_idx[0]].bbox

    for i in letter_idx[1:]:
        min_r, min_c, max_r, max_c = regions[i].bbox

        min_row, min_col = np.minimum([min_row, min_col], [min_r, min_c])
        max_row, max_col = np.maximum([max_row, max_col], [max_r, max_c])

    return (min_row, min_col, max_row, max_col)


def split_words(binary, regions) -> list:
    mean_letter_dist = mean_letter_distance(regions)

    text = []
    word_idx = [0]
    is_space = False

    for i in range(0, len(regions) - 1):
        x1 = regions[i].bbox[-1]
        x2 = regions[i + 1].bbox[1]

        if abs(x1 - x2) <= mean_letter_dist:

            # to check distance between `y1` and `y2`
            # would be good but not necessary right now
            if x1 >= x2:
                # gluing together intersecting symbols, i.e. `i`
                if isinstance(word_idx[-1], list):
                    word_idx[:-1].append(i + 1)
                else:
                    word_idx[-1] = [i, i + 1]
            else:
                word_idx.append(i + 1)
        else:
            is_space = True

        if is_space or i + 2 == len(regions):
            word = []

            for letter_idx in word_idx:
                if isinstance(letter_idx, list):
                    min_row, min_col, max_row, max_col = get_letter_boundaries(regions, letter_idx)
                    word.append(binary[min_row:max_row, min_col:max_col])
                else:
                    word.append(regions[letter_idx].image.astype("uint8"))

            text.append(word)

            if i + 1 != len(regions):
                word_idx = [i + 1]
            elif is_space:
                text.append([regions[i + 1].image.astype("uint8")])

            is_space = False
    return text


def image2text(img, knn) -> str:
    text = []

    binary = img.copy()
    binary[binary > 0] = 1

    labeled = label(binary)
    regions = sorted(regionprops(labeled), key=lambda region: region.bbox[-1])

    words = split_words(binary, regions)

    # predict letters
    for word_idx in range(len(words)):
        word = []
        for letter_idx in range(len(words[word_idx])):
            features = extract_features(words[word_idx][letter_idx])
            features = np.array(features, dtype=np.float32).reshape(1, len(features))
            ret, _, _, _ = knn.findNearest(features, k=3)
            word.append(chr(int(ret)))

        text.append(''.join(word))
        word.clear()

    return ' '.join(text)


def load_data(path) -> defaultdict:
    data = defaultdict(list)

    for p in sorted(path.glob("*")):
        if p.is_dir():
            data.update(load_data(p))
        else:
            gray = cv2.imread(str(p), 0)
            binary = gray.copy()
            binary[binary > 0] = 1
            data[path.name[-1]].append(binary)

    return data


def score(test_images, knn) -> float:
    score = 0.0

    for path in test_images:
        img = cv2.imread(str(path), 0)
        text = image2text(img, knn)

        if text == test_images[path]:
            score += 1
        else:
            error = 1 - sum([1 for i in range(len(text)) if text[i] == test_images[path][i]]) / len(text)
            print(f"Mismatch:\t`{text}` and `{test_images[path]}` -- error: {error:.2f}")

    return score / len(test_images)


if __name__ == "__main__":

    dir = Path.cwd() / "out"

    train_data = load_data(dir / "train")

    if len(train_data) == 0:
        print("Train data is empty")
    else:
        # Data preprocessing
        features = []
        responses = []

        for i, symbol in enumerate(train_data):
            # print(f"{symbol} # {i}")

            for img in train_data[symbol]:
                features.append(extract_features(img))
                responses.append(ord(symbol))

        features = np.array(features, dtype=np.float32)
        responses = np.array(responses, dtype=np.float32)

        # KNN
        knn = cv2.ml.KNearest_create()
        knn.train(features, cv2.ml.ROW_SAMPLE, responses)

        # Scoring
        phrases = [
            "C is LOW-LEVEL",
            "C++ is POWERFUL",
            "Python is INTUITIVE",
            "Rust is SAFE",
            "LUA is EASY",
            "Javascript is UGLY"
        ]

        test_images = {}
        for i in range(len(phrases)):
            test_images[dir / f"{i}.png"] = phrases[i]

        print(f"Score for {len(phrases)} test images: {score(test_images, knn):.2}")
